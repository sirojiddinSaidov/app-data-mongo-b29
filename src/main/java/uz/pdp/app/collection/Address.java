package uz.pdp.app.collection;

import lombok.Data;

@Data
public class Address {

    private String city;

    private String street;
}
