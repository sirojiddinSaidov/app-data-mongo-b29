package uz.pdp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDataMongoB29Application {

    public static void main(String[] args) {
        SpringApplication.run(AppDataMongoB29Application.class, args);
    }

}
