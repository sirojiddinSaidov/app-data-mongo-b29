package uz.pdp.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.pdp.app.collection.User;


public interface UserRepository extends MongoRepository<User, String> {


    @RestResource(path = "myPath")
    Page<User> findAllByNameContainingIgnoreCase(@Param("bla") String name, Pageable pageable);

}
